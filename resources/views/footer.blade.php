<div class="container">
    <footer class="my-5 text-muted text-center text-small">
        <p class="mb-1">© 2022 Mds-B3</p>
        <ul class="list-inline">
            <li class="list-inline-item"><a href="#">RGPD</a></li>
            <li class="list-inline-item"><a href="#">Mentions légales</a></li>
        </ul>
    </footer>
</div>