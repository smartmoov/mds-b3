<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Os extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'version',
    ];

    public function softwares()
    {
        return $this->belongsToMany(Software::class, 'os_softwares');
    }
}
