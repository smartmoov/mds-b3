<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Software extends Model
{
    use HasFactory;

    protected $table = 'softwares';

    protected $fillable = [
        'name',
        'description',
        'license',
    ];

    public function os()
    {
        return $this->belongsToMany(Os::class, 'os_softwares');
    }

}
