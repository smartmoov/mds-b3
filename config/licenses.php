<?php

return [
    'Public domain',
    'Permissive',
    'LGPL',
    'Copyleft',
    'Proprietary'
];
