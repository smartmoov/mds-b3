<?php

namespace Database\Seeders;

use App\Models\Os;
use Illuminate\Database\Seeder;

class OsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Os::factory(5)->create();
    }
}
