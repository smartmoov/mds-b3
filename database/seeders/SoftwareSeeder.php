<?php

namespace Database\Seeders;

use App\Models\Os;
use App\Models\Software;
use Illuminate\Database\Seeder;

class SoftwareSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $software = Software::factory(5)->create();
        Os::All()->each(function($os) use ($software){
            $os->softwares()->saveMany($software);
        });
    }
}
