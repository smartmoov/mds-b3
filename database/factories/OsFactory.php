<?php

namespace Database\Factories;

use App\Models\OS;
use Illuminate\Database\Eloquent\Factories\Factory;

class OsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OS::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'version' => (string) $this->faker->randomFloat(2,1,2),
        ];

    }
}
