<?php

namespace Database\Factories;

use App\Models\Software;
use App\Models\Os;
use Illuminate\Database\Eloquent\Factories\Factory;

class SoftwareFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Software::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'license' => collect(config('licenses'))->random(),
            'description' => $this->faker->text(30),
        ];

    }


}
