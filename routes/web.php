<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/graphics/user_by_os', function () {
    // $user = User::all();
    return view('home');
    // return view('graphics/user_by_os');
});

Route::get('/softwareList', function () {
    return view('softwareList');
});
